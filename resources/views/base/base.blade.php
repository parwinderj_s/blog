@include("partials._header")
@include("partials._nav")

@include("partials._messages")
@yield('page-content')


@include("partials._footer")