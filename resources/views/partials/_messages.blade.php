@if(Session::has("success"))

<div class="alert alert-success">
	<strong>{{Session::get('success')}}</strong>
</div>
@endif

@if(count($errors))

<div class="alert alert-danger">
	<strong>Errors:</strong>
	<ul>
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
	</ul>
</div>
@endif