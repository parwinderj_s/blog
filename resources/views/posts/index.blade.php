@extends('base.base')
@section('title', '| All posts')
@section("activeHome", "active")

@section('page-content')
	
	<div class="row">
	<div class="col-md-10">
		<h1>Posts</h1>
	</div>
	<div class="col-md-2 h1-parallel">
		<a href="{{route('posts.create')}}" class="btn btn-primary btn-block">Create New</a>
	</div>
	</div>
		
    <div class="row">
        <div class="col-md-12">

      
      	<table class="table">
      		<thead>
      			<th>#</th>
      			<th>Title</th>
      			<th>Body</th>
      			<th>Created At</th>
      			<th>Updated At</th>
      		</thead>
      		<tbody>
      			@foreach($posts as $post)
      			<tr>
      				<td>{{$post->id}}</td>
					<td>{{$post->title}}</td>
					<td>{{substr($post->body, 0, 20)}}{{(strlen($post->body)>20)?"  ...":""}}</td>
					<td>{{date('d M Y H:i a',strtotime($post->created_at))}}</td>
					<td>{{date('d M Y H:i a',strtotime($post->updated_at))}}</td>
					<td><a href="{{ route('posts.show', ['id' => $post->id]) }}" class="btn btn-default">Read</a>
					<a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default">Edit</a>
					</td>
      			</tr>
      			@endforeach
      		</tbody>
      	</table>
        </div>
    </div>
@endsection