@extends("base.base")
@section("activeHome", "active")


@section("page-content")
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h1>Create New Post</h1>	
		<hr>
		<form action="{{ route('posts.store') }}" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <div class="form-group">
		    <label for="title">Title</label>
		    <input type="text" class="form-control" id="title" placeholder="Title" name="title">
		  </div>
		  <div class="form-group">
		  	<label for="body">Body</label>
		  	<textarea class="form-control" id="body" name="body"></textarea>
		  </div>
		  <button type="submit" class="btn btn-default" id="submit1">Submit</button>
		</form>
	</div>
	</div>
@endsection
