@extends("base.base")
@section("title", "| show posts")
@section("activeHome", "active")

@section("page-content")
<div class="row">
	<div class="col-md-8">
		<h1>{{$post->title}}</h1>
		<p>{{$post->body}}</p>	
	</div>

	<div class="col-md-4">
		<div class="well">
			<dl class="dl-horizontal">
				<dt>Created At:</dt>
				<dd>{{date('d M Y H:i a',strtotime($post->created_at))}}</dd>
			</dl>
			<dl class="dl-horizontal">
				<dt>Updated At</dt>
				<dd>{{date('d M Y H:i a',strtotime($post->updated_at))}}</dd>
			</dl>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<a href="{{ route('posts.edit', ['id' => $post->id]) }}" class="btn btn-primary btn-block">Edit</a>
				</div>
				<div class="col-sm-6">
					<form method="post" action="{{ route('posts.destroy', ['id' => $post->id]) }}">
					{!! csrf_field() !!}
					<input type="hidden" name="_method" value="DELETE">
					<input type="submit" class="btn btn-danger btn-block" value="Delete">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
	
@endsection