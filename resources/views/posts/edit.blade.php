@extends("base.base")
@section('title', "| Edit post")
@section('activeHome', "active")

@section('page-content')
	<div class="row">
	<div class="col-md-8">
		<form method="post" action="{{ route('posts.update', ['id' => $post->id]) }}">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT"></input>
		<label for="title">Title</label>
		<input type='text' class='form-control' name= "title" value="{{ $post->title }}">
		<label for="body">Body</label>
		<textarea id="body" cols="30" rows="10" class="form-control" name="body">{{ $post->body }}</textarea>
			
		
	</div>

	<div class="col-md-4">
		<div class="well">
			<dl class="dl-horizontal">
				<dt>Created At:</dt>
				<dd>{{date('d M,Y H:i a',strtotime($post->created_at))}}</dd>
			</dl>
			<dl class="dl-horizontal">
				<dt>Updated At</dt>
				<dd>{{date('d M,Y H:i a',strtotime($post->updated_at))}}</dd>
			</dl>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<a href="{{ route('posts.show', ['id' => $post->id]) }}" class="btn btn-danger btn-block">Cancel</a>
				</div>
				<div class="col-sm-6">
					<input type="submit" class="btn btn-success btn-block" name="submit" value="Save">
				</div>
			</div>
		</div>
	</div>
	</form> <!-- end of form -->
	</div>
@endsection